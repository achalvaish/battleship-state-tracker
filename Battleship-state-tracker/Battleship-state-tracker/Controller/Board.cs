﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship_state_tracker.Models;
using Battleship_state_tracker.Resources;

namespace Battleship_state_tracker.Controller
{
    public class Board
    {
        public int _boardSize { get; set; }
        public List<List<BoardCell>> _board { get; set; }

        public List<Ship> _ships{get; set;}

        public Board(int size)
        {
            _board = new List<List<BoardCell>>();
            _ships = new List<Ship>();

            SetupBoard(size);
        }

        private void SetupBoard(int boardSize)
        {
            _boardSize = boardSize;

            for (int xPos = 0; xPos < _boardSize; xPos++)
            {
                _board.Add(new List<BoardCell>());

                for (int yPos = 0; yPos < _boardSize; yPos++)
                {
                    _board[xPos].Add(new BoardCell(xPos, yPos));
                }
            }

        }

        public bool AddShip(BoardCell startPosition, Orientation orientation, int length)
        {
            if (IsValidPlacement(startPosition, orientation, length))
            {
                Ship ship = new Ship(startPosition, orientation, length);
                if (orientation == Orientation.Horiztontal)
                {
                    AddHorizontally(ship);
                    return true;
                }
                else
                {
                    AddVertictally(ship);
                    return true;
                }

            }
            else
            {
                // Invalid placement message: Cell not available
                Console.WriteLine(Constants.INVALID_PLACEMENT);
                return false;
            }            

        }

        private bool IsValidPlacement(BoardCell startPosition,Orientation orientation, int length)
        {
            if(orientation == Orientation.Vertical)
            {
                int yPos = startPosition._col;
                for(int xPos = startPosition._row; length!=0; ++xPos, --length)
                {
                    if (!checkCoordinatesValidity(xPos,yPos) || !(_board[xPos][yPos]._type == BoardCellType.Available))
                        return false;
                }

                return true;
            }
            else
            {                
                int xPos = startPosition._row;
                for (int yPos = startPosition._col; length!=0; ++yPos,--length)
                {
                    if (!checkCoordinatesValidity(xPos, yPos) || !(_board[xPos][yPos]._type == BoardCellType.Available))
                        return false;
                }

                return true;
            }
            
        }

        private void AddHorizontally(Ship ship)
        {
            int xPos = ship._startPosition._row;
            int length = ship._length;
            for (int yPos = ship._startPosition._col; length!=0; ++yPos,--length)
            {
                this._board[xPos][yPos]._type = BoardCellType.Occupied;
                this._board[xPos][yPos]._shipIndex = _ships.Count;
            }

            _ships.Add(ship);
            Console.WriteLine("Ship added");
        }

        private void AddVertictally(Ship ship)
        {
            int yPos = ship._startPosition._col;
            int length = ship._length;
            for (int xPos = ship._startPosition._col; length!=0; ++xPos,--length)
            {
                this._board[xPos][yPos]._type = BoardCellType.Occupied;
                this._board[xPos][yPos]._shipIndex = _ships.Count;
            }

            _ships.Add(ship);
            Console.WriteLine("Ship added");
        }

        public void Attack(int x,int y)
        {
            if (checkCoordinatesValidity(x, y))
            {
                var attackedShip = _ships[_board[x][y]._shipIndex];

                switch (_board[x][y]._type)
                {
                    case BoardCellType.Occupied: 
                        attackedShip.Damaged(_board[x][y]);
                        if (attackedShip.isSunk)
                        {
                            _ships.Remove(attackedShip);
                            Console.WriteLine("Ship has been sunk");
                        }
                        else
                        {
                            Console.WriteLine("Ship has been hit");                            
                        }
                        
                        break;
                    case BoardCellType.Available: // Message: No ship at this cell (miss) 
                        Console.WriteLine("No ship exists at this lcoation (miss)");
                        break;
                    case BoardCellType.Damaged:
                        if (attackedShip != null)
                        {
                            // Message: Ship already damaged
                            Console.WriteLine("Ship already damaged at this location");
                        }
                        else
                        {
                            // Message: Ship has been sunk already
                            Console.WriteLine("Ship has already been sunk at this location");
                        }
                        
                        break;
                    default: //Error with the attack
                        Console.WriteLine("An error occurred with the attack");
                        break;

                }
            }
            else
            {
                Console.WriteLine(Constants.INVALID_PLACEMENT);
            }
        }

        private bool checkCoordinatesValidity(int row, int col)
        {
            if (row < _boardSize && col < _boardSize && row >= 0 && col >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
