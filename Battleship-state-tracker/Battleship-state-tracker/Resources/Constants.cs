﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_state_tracker.Resources
{
    static class Constants
    {
        public const int BOARDSIZE = 10;
        public const string INVALID_PLACEMENT = "Ship cannot be placed. Please try adding to a new location";
    }
}
