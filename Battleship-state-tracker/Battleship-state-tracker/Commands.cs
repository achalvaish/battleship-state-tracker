﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship_state_tracker.Models;
using Battleship_state_tracker.Controller;
using Battleship_state_tracker.Resources;

namespace Battleship_state_tracker
{
    public class Commands
    {
        private bool checkArgumentsSetup(string[] commandArguments, int totalArgs)
        {
            if (commandArguments.Length == totalArgs)
            {
                if (!int.TryParse(commandArguments[0], out int x))
                    return false;

                if (!int.TryParse(commandArguments[1], out int y))
                    return false;

                switch (commandArguments[2].ToLower())
                {
                    case "vertical":
                        break;
                    case "horizontal":
                        break;
                    default:
                        return false;
                }

                if (!int.TryParse(commandArguments[3], out int length))
                    return false;

                return true;
            }
            else
            {
                return false;
            }
        }

        public void executeSetup(Board board)
        {
            string command;
            DisplaySetupCommands();

            command = Console.ReadLine();
            if (!String.IsNullOrEmpty(command))
            {
                while (!command.Equals('D'))
                {                    
                    string[] commandArgs = command.Split(' ');
                    if (checkArgumentsSetup(commandArgs, 4))
                    {
                        int x = Convert.ToInt32(commandArgs[0]);
                        int y = Convert.ToInt32(commandArgs[1]);
                        Orientation orientation = commandArgs[2].ToLower() == "vertical" ? Orientation.Vertical : Orientation.Horiztontal;
                        int length = Convert.ToInt32(commandArgs[3]);

                        BoardCell shipCell = new BoardCell(x, y);
                        board.AddShip(shipCell, orientation, length);                        
                    }
                    else
                    {
                        Console.WriteLine(Constants.INVALID_PLACEMENT);
                    }

                    command = Console.ReadLine();
                    while (String.IsNullOrEmpty(command))
                    {
                        command = Console.ReadLine();
                        continue;
                    }
                }
            }
        }

        static void DisplaySetupCommands()
        {
            Console.WriteLine("Add Ships in the format: x y orientation length\n");
            Console.WriteLine("with x: number (no decimal), y: number (no decimal)\n");
            Console.WriteLine("orientation: 'vertical' or 'hozirontal'(without ''), length: number (no decimal)\n");
            Console.WriteLine("Press D when all ships are placed");
        }

        private bool checkArgumentsAttack(string[] commandArguments, int totalArgs)
        {
            if (commandArguments.Length == totalArgs)
            {
                bool isNumeric = false;
                for (int i = 1; i < totalArgs; i++)
                {
                    isNumeric = int.TryParse(commandArguments[i], out int x);
                    if (isNumeric == false)
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void executeAttack(Board board)
        {
            string command;
            DisplayAttackCommands();

            command = Console.ReadLine();
            if (!String.IsNullOrEmpty(command))
            {
                while(board._ships.Count > 0)
                {
                    if (String.IsNullOrEmpty(command))
                    {
                        command = Console.ReadLine();
                        continue;
                    }
                    string[] commandArgs = command.Split(' ');
                    if (checkArgumentsAttack(commandArgs, 2)) {
                        int x = Convert.ToInt32(command[1]);
                        int y = Convert.ToInt32(command[2]);

                        board.Attack(x, y);
                    }
                    else
                    {
                        //Invalid args exception
                        Console.WriteLine(Constants.INVALID_PLACEMENT);
                    }
                    
                }

                if(board._ships.Count == 0)
                {
                    // All ships have been sunk
                    Console.WriteLine("All ships have been sunk");
                }
            }

        }
        static void DisplayAttackCommands()
        {
            Console.WriteLine("Attack ships in the format: x y");
            Console.WriteLine("with x: number (no decimal), y: number (no decimal)\n");            
        }
    }
}
