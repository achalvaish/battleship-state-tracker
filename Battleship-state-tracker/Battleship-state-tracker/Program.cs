﻿using System;
using Battleship_state_tracker.Resources;
using Battleship_state_tracker.Controller;

namespace Battleship_state_tracker
{
    class Program
    {
        static void Main(string[] args)
        {
            Commands commands = new Commands();

            Console.WriteLine("Battleship state tracker");
            //Create Board
            Board board = new Board(Constants.BOARDSIZE);
            Console.WriteLine("Board created with size {0}\n",Constants.BOARDSIZE);
            //Setup board
            Console.WriteLine("SETUP MODE\n");
            commands.executeSetup(board);

            //Attack mode
            Console.WriteLine("ATTACK MODE\n");
            commands.executeAttack(board);
        }
        
    }

}