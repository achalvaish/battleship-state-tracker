﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_state_tracker.Models
{

    public enum Orientation
    {
        Vertical = 0,
        Horiztontal = 1
    };

    interface IShip
    {        
        void Damaged(BoardCell cell);

    }

    public class Ship:IShip
    {
        public int _health;
        public int _length;
        public BoardCell _startPosition;        
        public Orientation _orientation;
        public bool isSunk { get; set; }

        public Ship(BoardCell cell, Orientation orientation, int length)
        {
            _startPosition = cell;
            _orientation = orientation;            
            _health = length;
            _length = length;
        }

        public void Damaged(BoardCell cell)
        {
            _health--;
            isSunk = _health == 0;
            cell._type = BoardCellType.Damaged;
        }

    }
}
