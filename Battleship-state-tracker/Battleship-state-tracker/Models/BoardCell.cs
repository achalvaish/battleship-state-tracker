﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_state_tracker.Models
{

    public enum BoardCellType
    {    
        Available,
        Occupied,
        Damaged
    }

    public class BoardCell
    {
        public int _row { get; set; }
        public int _col { get; set; }
        public int _shipIndex { get; set; }
        public BoardCellType _type { get; set; }

        public BoardCell(int row, int col)
        {
            this._row = row;
            this._col = col;
            this._type = BoardCellType.Available;
        }

        public void ResetCell(BoardCellType type)
        {
            this._type = type;
            this._shipIndex = -1;
        }
    }
}
