using Battleship_state_tracker.Controller;
using Battleship_state_tracker.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Battleship_Test
{
    [TestClass]
    public class BattleshipTests
    {
        [TestMethod]
        public void Create_BoardCell_Expect_CorrectCellCreation()
        {
            BoardCell ship1Cell = new BoardCell(5, 6);

            Assert.IsNotNull(ship1Cell);
        }

        [TestMethod]
        public void Create_BoardCell_Expect_CorrectCoordinates()
        {
            BoardCell ship1Cell = new BoardCell(5, 6);

            Assert.AreEqual(5, ship1Cell._row);
            Assert.AreEqual(6, ship1Cell._col);
        }

        [TestMethod]
        public void Create_Board_Expect_CorrectBoardCreation()
        {
            Board game = new Board(10);

            Assert.IsNotNull(game._board);
        }

        [TestMethod]
        public void Create_Board_Expect_CorrectRowNumber()
        {
            Board game = new Board(10);

            Assert.AreEqual(game._board.Count, 10);
        }

        [TestMethod]
        public void Create_Board_Expect_CorrectColNumber()
        {
            Board game = new Board(10);

            for (int i = 0; i != game._boardSize; ++i)
            {
                Assert.AreEqual(game._board[i].Count, 10);
            }
        }

        [TestMethod]
        public void Initialize_BoardWithWater_ExpectAllCellsToBeWater()
        {
            Board game = new Board(10);

            for (int i = 0; i != game._boardSize; ++i)
            {
                for (int j = 0; j != game._boardSize; ++j)
                {
                    Assert.AreEqual(game._board[i][j]._type, BoardCellType.Available);
                }
            }
        }

        [TestMethod]
        public void Create_Ship_Expect_CorrectCreation()
        {
            BoardCell ship1Cell = new BoardCell(5, 6);
            Ship ship = new Ship(ship1Cell, Orientation.Horiztontal, 3);

            Assert.IsNotNull(ship);
        }

        [TestMethod]
        public void Create_Ship_Expect_CorrectHeadPosition()
        {
            BoardCell ship1Cell = new BoardCell(5, 6);
            Ship ship = new Ship(ship1Cell, Orientation.Horiztontal, 3);

            Assert.AreEqual(ship._startPosition._row, 5);
            Assert.AreEqual(ship._startPosition._col, 6);
        }

        [TestMethod]
        public void Create_Ship_Expect_CorrectOrientation()
        {
            BoardCell ship1Cell = new BoardCell(5, 6);
            Ship ship = new Ship(ship1Cell, Orientation.Horiztontal, 3);

            Assert.AreEqual(ship._orientation, Orientation.Horiztontal);
        }


        [TestMethod]
        public void Add_VerticalShipToBoard_Expect_CorrectPlacement()
        {
            Board game = new Board(10);

            BoardCell ship1Cell = new BoardCell(5, 6);
            Ship ship = new Ship(ship1Cell, Orientation.Horiztontal, 3);

            Assert.AreEqual(ship._orientation, Orientation.Horiztontal);
        }

        [TestMethod]
        public void Add_HorizontalShipToBoard_Expect_AddedShip()
        {
            Board game = new Board(10);

            BoardCell ship1Cell = new BoardCell(5, 6);

            game.AddShip(ship1Cell, Orientation.Horiztontal, 3);

            Assert.AreEqual(game._ships.Count, 1);
        }

        [TestMethod]
        public void Add_VerticalShipToBoard_Expect_AddedShip()
        {
            Board game = new Board(10);

            BoardCell ship1Cell = new BoardCell(5, 6);

            game.AddShip(ship1Cell, Orientation.Vertical, 3);

            Assert.AreEqual(game._ships.Count, 1);
        }

        [TestMethod]
        public void Add_TooLongVerticalShipToBoard_Expect_OutOfBoardErrorMessage()
        {
            Board game = new Board(2);

            BoardCell ship1Cell = new BoardCell(1, 1);

            var returnValue = game.AddShip(ship1Cell, Orientation.Vertical, 15);

            Assert.AreEqual(returnValue, false);
        }

        [TestMethod]
        public void Add_TooLongHorizontalShipToBoard_OutOfBoardErrorMessage()
        {
            Board game = new Board(10);

            BoardCell ship1Cell = new BoardCell(5, 6);

            var returnValue = game.AddShip(ship1Cell, Orientation.Horiztontal, 15);

            Assert.AreEqual(returnValue, false);
        }

        [TestMethod]
        public void Add_SeveralValidShips_Expect_CorrectCountAddedShips()
        {
            Board game = new Board(100);

            BoardCell ship1Cell = new BoardCell(1, 1);
            BoardCell ship2Cell = new BoardCell(10, 10);
            BoardCell ship3Cell = new BoardCell(20, 20);
            BoardCell ship4Cell = new BoardCell(30, 30);

            game.AddShip(ship1Cell, Orientation.Vertical, 4);
            game.AddShip(ship2Cell, Orientation.Horiztontal, 3);
            game.AddShip(ship3Cell, Orientation.Vertical, 5);
            game.AddShip(ship4Cell, Orientation.Horiztontal, 2);

            Assert.AreEqual(game._ships.Count, 4);
        }

        [TestMethod]
        public void Add_InvalidShips_Expect_InvalidCoordinatesError()
        {
            Board game = new Board(10);

            BoardCell ship1Cell = new BoardCell(15, 12);
            bool returnValue = game.AddShip(ship1Cell, Orientation.Vertical, 4);

            Assert.AreEqual(returnValue, false);
        }

        [TestMethod]
        public void Add_ShipOnAlreadyPlacedShip_Expect_NoCellAvailableError()
        {
            Board game = new Board(10);

            BoardCell ship1Cell = new BoardCell(1, 1);
            BoardCell ship2Cell = new BoardCell(1, 1);

            game.AddShip(ship1Cell, Orientation.Vertical, 3);
            bool returnValue = game.AddShip(ship2Cell, Orientation.Vertical, 3);

            Assert.AreEqual(returnValue, false);
        }

        [TestMethod]
        public void Add_Ship_WhenShipOverlay_Expect_NoCellAvailableError()
        {
            Board game = new Board(50);

            BoardCell ship1Cell = new BoardCell(20, 20);
            BoardCell ship2Cell = new BoardCell(18, 22);

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            bool returnValue = game.AddShip(ship2Cell, Orientation.Vertical, 5);

            Assert.AreEqual(returnValue, false);
        }

        [TestMethod]
        public void Add_Ship_OnAlreadySunkShip_Expect_NoCellAvailableError()
        {
            Board game = new Board(50);

            BoardCell ship1Cell = new BoardCell(20, 20);
            BoardCell ship2Cell = new BoardCell(18, 22);

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            game.Attack(20, 20);
            game.Attack(20, 21);
            game.Attack(20, 22);
            game.Attack(20, 23);
            game.Attack(20, 24);
            //ship1 is now sunk
            Assert.AreEqual(game._ships.Count, 0);
            bool returnValue = game.AddShip(ship2Cell, Orientation.Vertical, 5);

            Assert.AreEqual(returnValue, false);
        }

        [TestMethod]
        public void Attack_Ship_Expect_ShipToBeDamaged()
        {
            Board game = new Board(10);
            BoardCell ship1Cell = new BoardCell(1, 1);
            int x = 1;
            int y = 2;

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            int shipHealth = game._ships[0]._health;

            game.Attack(x, y);

            Assert.AreEqual(game._ships[0]._health, shipHealth - 1);
        }

        [TestMethod]
        public void Attack_Ship_Expect_CellUpdated()
        {
            Board game = new Board(10);
            BoardCell ship1Cell = new BoardCell(1, 1);
            int x = 1;
            int y = 2;

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            int shipHealth = game._ships[0]._health;

            game.Attack(x, y);

            Assert.AreEqual(game._board[x][y]._type, BoardCellType.Damaged);
        }

        [TestMethod]
        public void Attack_Ship_Expect_CellAroundDamageToBeUndamaged()
        {
            Board game = new Board(10);
            BoardCell ship1Cell = new BoardCell(1, 1);
            int x = 1;
            int y = 2;

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            int shipHealth = game._ships[0]._health;

            game.Attack(x, y);

            Assert.AreEqual(game._board[x][y - 1]._type, BoardCellType.Occupied);
            Assert.AreEqual(game._board[x][y]._type, BoardCellType.Damaged);
            Assert.AreEqual(game._board[x][y + 1]._type, BoardCellType.Occupied);
        }

        [TestMethod]
        public void Attack_Ship_Expect_ShipToBeDamagedButNotSunk()
        {
            Board game = new Board(10);
            BoardCell ship1Cell = new BoardCell(1, 1);
            int x = 1;
            int y = 2;

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            int shipHealth = game._ships[0]._health;

            game.Attack(x, y);

            Assert.AreEqual(game._ships[0].isSunk, false);
        }

        [TestMethod]
        public void Attack_AllShipCells_Expect_ShipToBeSunk()
        {
            Board game = new Board(10);
            BoardCell ship1Cell = new BoardCell(1, 1);
            int x = 1;
            int y = 1;

            game.AddShip(ship1Cell, Orientation.Horiztontal, 5);
            int shipHealth = game._ships[0]._health;

            // Carrier has 5 cells
            game.Attack(x, y);
            game.Attack(x, y + 1);
            game.Attack(x, y + 2);
            game.Attack(x, y + 3);
            game.Attack(x, y + 4);

            Assert.AreEqual(game._ships.Count, 0);
        }

        [TestMethod]
        public void Attack_WaterCell_Expect_FailAttackAndWaterCell()
        {
            Board game = new Board(10);
            BoardCell ship1Cell = new BoardCell(1, 1);
            int x = 8;
            int y = 9;

            game.AddShip(ship1Cell, Orientation.Horiztontal, 3);

            game.Attack(x, y);

            Assert.AreEqual(game._board[x][y]._type, BoardCellType.Available);
        }

    }
}
