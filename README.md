# BattleShip State Tracker #

Console application for tracking state for a single player Battlship game

### What is this repository for? ###

This repository contains the VS solution for battlehip state tracker along with test project.

### How do I get set up? ###

* Summary of set up
	* Navigate to the Battleship-state-tracker folder to locate the VS solution file.
	* Open the VS solution file to find both the apllication and the test project

* Configuration
    *  Microsoft Visual Studio 2022

* Dependencies
	* Microsoft.NETCore.App
	* MSTest.TestFramework(2.2.7)
	* MSTest.TestAdapter(2.2.7)

* How to run tests
	* Locate the Battleship-Test project
	* Right click on the project and select Run Tests